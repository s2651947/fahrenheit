package nl.utwente.di.fahrenheit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuote {
    @Test
    public void testBook1() throws Exception{
        Converter converter = new Converter();
        double price = converter.getBookPrice("1");
        Assertions.assertEquals(10.0, price , 0.0, "Proce of book 1");
    }
}
