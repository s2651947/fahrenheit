package nl.utwente.di.fahrenheit;

public class Converter {
    double getBookPrice(String isbn){
        return Double.parseDouble(isbn)*1.8+32;
    }
}
